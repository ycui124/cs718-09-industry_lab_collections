package ictgradschool.industry.lab_collections.example1;

public class Text {
    public static void main(String[] args) {
        Box<Animal> box5=new Box<>();
        box5.set(new TRex());
        System.out.println(box5.get());
        TRex t= (TRex) box5.get();
        System.out.println(t);
    }
}
