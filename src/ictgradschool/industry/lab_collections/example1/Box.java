package ictgradschool.industry.lab_collections.example1;

public class Box<T> {
    private T thing;

    public void set(T thing) {
        this.thing = thing;
    }

    public T get() {
        return thing;
    }
}
