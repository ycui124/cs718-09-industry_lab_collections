package ictgradschool.industry.lab_collections.ex01;

import java.awt.*;
import java.util.ArrayList;

public class ExerciseOne {
    public void exA() {
        ArrayList list = new ArrayList();
        Character letter = new Character('a');
        list.add(letter);
        if (list.get(0).equals("a")) {
            System.out.println("funny");
        } else {
            System.out.println("Not funny");
        }
    }

    public void exB() {
        ArrayList<Point> list = new ArrayList<Point>();
        Point pt1 = new Point(3, 4);
        list.add(pt1);
        Point pt2 = list.get(0);
        pt2.x = 23;
        if (pt2 == pt1) {
            System.out.println("Same object");
            System.out.println(pt1.x+"  "+pt2.x);
        } else {
            System.out.println("Different object");
        }
    }
public void exC(){
    ArrayList list = new ArrayList();
    list.add('a');
    list.add(0, 34);
//    String c1 = (String) list.get(1);
    System.out.println(list.get(0)+"  "+list.get(1));
}
    public static void main(String[] args) {
        ExerciseOne eo = new ExerciseOne();
        eo.exA();
        eo.exB();
        eo.exC();
    }
}
