package ictgradschool.industry.lab_collections.example2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Text {
    public static void main(String[] args) {
        List<String> alist=new ArrayList<>();
        alist.add("Anna");
        alist.add("Liza");
        alist.add("Yang");
        alist.add("Alex");
        alist.add("Bella");
        alist.add("Nick");
        Set<String> hset=new HashSet<>();
        hset.add("Anna");
        hset.add("Liza");
        hset.add("Yang");
        hset.add("Alex");
        hset.add("Bella");
        hset.add("Nick");
        for (String ele:alist) {
            System.out.println(ele);
        }
        System.out.println();
        for (String ele: hset) {
            System.out.println(ele);
        }
    }
}
