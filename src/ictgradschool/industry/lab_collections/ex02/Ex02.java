package ictgradschool.industry.lab_collections.ex02;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Ex02 {
    public static void main(String[] args) {
        List<String> myList = new ArrayList();
        String[] array = {"ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN"};
        for (int i=0;i<=array.length-1;i++){
            myList.add(array[i]);
            System.out.println(myList.get(i));
        }

        System.out.println();

        //    +++++++++++++++++++++++++++++
        for (int i = 0; i <array.length ; i++) {
//            System.out.println(myList.get(i).toLowerCase());
            myList.set(i,array[i].toLowerCase());
        }

        System.out.println(myList);


        System.out.println("==============");
        int i=0;
        for (String element : myList) {
            myList.set(i,array[i].toLowerCase());
            i++;
//            System.out.println(element);
        }
        System.out.println(myList);

        System.out.println();
        int j=-1;
        Iterator<String> myIter = myList.iterator();
        while (myIter.hasNext()) {
            String element=myIter.next();
            myList.set(j+1,element.toLowerCase());
            j++;
//            System.out.println(element);
        }
        System.out.println(myList);
    }
}


